# Trunic Parser:
Parser that takes written english and transliterates it to Trunic, a constructed script created by
Andrew Shouldice for the game Tunic.

Simply input a string of English (punctuation not currently supported) and a Trunic transliteration will be generated
for you.
Due to inconsistencies with Shouldice's own transliteration, transliterations are accurate but may not match perfectly
with those present in Tunic.

## Example:
Taken from Tunic's Steam description:

"Explore a land filled with lost legends ancient powers and ferocious monsters in TUNIC an isometric action game
about a small fox on a big adventure"

![Trunic Example](images/trunic_example.png)

### More info:
This script uses Python and John Zelle's `graphics.py` TKinter wrapper.

To run the script, run `trunic_parser.py` in your Python (3 or greater) implementation.
It will then ask for a query, which will show the resulting phonemes fetched from Carnegie Mellon's phoneme database.