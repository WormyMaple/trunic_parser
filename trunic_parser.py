import graphics as gl
import time
import phoneme_checker

def multiply_point(point, m):
    return gl.Point(point.getX() * m, point.getY() * m)

def add_point(p1, p2):
    return gl.Point(p1.getX() + p2.getX(), p1.getY() + p2.getY())

def draw_filler(p):
    new_circle = gl.Circle(p, line_width / 2 - 1)
    new_circle.setFill("black")
    new_circle.draw(gl_win)
    return new_circle

def draw_phoneme(phoneme, draw_pos, draw_line, circle):
    drawn_objects = []

    for line in phoneme:
        p1_point = phoneme_points[line[0]]
        p2_point = phoneme_points[line[1]]
        p1 = add_point(multiply_point(p1_point, draw_scale), draw_pos)
        p2 = add_point(multiply_point(p2_point, draw_scale), draw_pos)

        new_line = gl.Line(p1, p2)
        new_line.setWidth(line_width)
        new_line.draw(gl_win)
        drawn_objects.append(new_line)

        drawn_objects.append(draw_filler(p1))
        drawn_objects.append(draw_filler(p2))
    
    if circle:
        new_circle = gl.Circle(add_point(multiply_point(phoneme_points[-1], draw_scale), draw_pos), circle_width * draw_scale)
        new_circle.setWidth(line_width)
        new_circle.draw(gl_win)
        drawn_objects.append(new_circle)

    if draw_line:
        line_p1 = add_point(multiply_point(word_line_points[0], draw_scale), draw_pos)
        line_p2 = add_point(multiply_point(word_line_points[1], draw_scale), draw_pos)
        line = gl.Line(line_p1, line_p2)
        line.setWidth(line_width)
        line.draw(gl_win)
        drawn_objects.append(line)
        drawn_objects.append(draw_filler(line_p1))
        drawn_objects.append(draw_filler(line_p2))

    return drawn_objects

bounds = [1280, 720]
max_draw_pos = bounds[0] / 4

draw_scale = 12
line_width = 0.5 * draw_scale
circle_width = 0.5
space_dist = 2 * draw_scale
line_height = 10 * draw_scale

gl_win = gl.GraphWin("Trunic", bounds[0], bounds[1], autoflush=False)
gl_win.setBackground("White")
b_2 = (bounds[0] / 2, bounds[1] / 2)
gl_win.setCoords(-b_2[0], -b_2[1], b_2[0], b_2[1])

phoneme_points = [gl.Point(0, 3), 
                  gl.Point(-2, 2 - 0.25), gl.Point(2, 2 - 0.25),
                  gl.Point(0, 0.5),
                  gl.Point(-2, 0), gl.Point(0, 0), gl.Point(2, 0),
                  gl.Point(-2, -0.75), gl.Point(0, -0.75), gl.Point(2, -0.75),
                  gl.Point(-2, -2), gl.Point(2, -2),
                  gl.Point(0, -3 - 0.25),
                  gl.Point(0, -4)]

word_line_points = [gl.Point(-2, 0), gl.Point(2, 0)]

phonemes = {
    "AE": [(4, 1), (1, 0), (0, 2), (7, 10)],
    "AA R": [(1, 0), (0, 2), (10, 12), (12, 11)],
    "AA": [(4, 1), (1, 0), (7, 10)],
    "AO": [(4, 1), (1, 0), (7, 10)],
    "EY": [(0, 1)],
    "EH": [(4, 1), (7, 10), (10, 12), (12, 11)],
    "IY": [(4, 1), (1, 0), (7, 10), (10, 12), (12, 11)],
    "IY R": [(4, 1), (1, 0), (7, 10), (12, 11)],
    "IH R": [(4, 1), (1, 0), (7, 10), (12, 11)],
    "AH": [(1, 0), (0, 2)],
    "EH R": [(4, 1), (7, 10), (12, 11)],
    "IH": [(10, 12), (12, 11)],
    "AY": [(0, 2)],
    "ER": [(4, 1), (0, 2), (7, 10), (10, 12), (12, 11)],
    "OW": [(4, 1), (1, 0), (0, 2), (7, 10), (10, 12), (12, 11)],
    "OY": [(10, 12)],
    "UW": [(4, 1), (1, 0), (0, 2), (7, 10), (10, 12)],
    "UH": [(4, 1), (7, 10), (10, 12)],
    "AW": [(12, 11)],
    "AO R": [(4, 1), (1, 0), (0, 2), (7, 10), (12, 11)],

    "B": [(5, 0), (8, 11)],
    "CH": [(5, 3), (3, 1), (8, 12)],
    "D": [(5, 0), (8, 10), (8, 11)],
    "F": [(5, 3), (3, 2), (8, 10), (8, 12)],
    "G": [(5, 3), (3, 2), (8, 12), (8, 11)],
    "HH": [(5, 0), (8, 12), (8, 11)],
    "JH": [(5, 0), (8, 10)],
    "K": [(5, 0), (3, 2), (8, 11)],
    "L": [(5, 0), (8, 12)],
    "M": [(8, 10), (8, 11)],
    "N": [(3, 1), (8, 10), (8, 11)],
    "NG": [(5, 0), (3, 1), (3, 2), (8, 10), (8, 12), (8, 11)],
    "P": [(5, 3), (3, 2), (8, 12)],
    "R": [(5, 0), (3, 2), (8, 12)],
    "S": [(5, 0), (3, 2), (8, 10), (8, 12)],
    "SH": [(5, 3), (3, 1), (3, 2), (8, 10), (8, 12), (8, 11)],
    "T": [(5, 3), (3, 1), (3, 2), (8, 12)],
    "TH": [(5, 0), (3, 1), (3, 2), (8, 12)],
    "DH": [(5, 0), (8, 10), (8, 12), (8, 11)],
    "V": [(5, 0), (3, 1), (8, 11)],
    "W": [(3, 1), (3, 2)],
    "Y": [(5, 0), (3, 1), (8, 12)],
    "Z": [(5, 0), (3, 1), (8, 12), (8, 11)],
    "ZH": [(5, 0), (3, 1), (3, 2), (8, 10), (8, 11)]
}

vowels = ["AE", "AA", "EY", "EH", "IY", "AH", "IH", "AY", "ER", "OW", "OY", "UW", "UH", "AW", "AO"]
combined_phonemes = ["AA1 R", "IY1 R", "IH1 R", "EH1 R", "AO1 R"]
vowels.append(combined_phonemes)
consonants = ["B", "CH", "D", "F", "G", "HH", "JH", "K", "L", "M", "N", "P", "R", "S", "SH", "T", "TH", "DH", "V", "W", "Y", "Z", "ZH"]
singled_vowels = ["ER"]


phoneme_checker.start_database()

drawn_objects = []
draw_pos = gl.Point(-b_2[0] + 4 * draw_scale, b_2[1] - 5 * draw_scale)

written_phonemes = phoneme_checker.query()
print(written_phonemes)
split_phonemes = written_phonemes.split()
previous_was_vowel = False
phonemes_drawn = 0

def check_pos():
    if draw_pos.x > max_draw_pos:
        draw_pos.x = -b_2[0] + 4 * draw_scale
        draw_pos.y -= line_height


for i in range(len(split_phonemes)):
    phoneme = split_phonemes[i]

    next_is_vowel = False
    next_is_space = False
    is_vowel = phoneme in vowels

    if i < len(split_phonemes) - 1:
        next_is_vowel = split_phonemes[i + 1] in vowels
        next_is_space = split_phonemes[i + 1] == "_"

    if phoneme != "_":
        to_draw = phonemes[phoneme]

        if phonemes_drawn == 0:
            draw_phoneme(to_draw, draw_pos, True, False)

            if not next_is_vowel and (not is_vowel or phoneme in singled_vowels):
                phonemes_drawn = -1
                draw_pos.x += 4 * draw_scale

        elif phonemes_drawn == 1:
            if previous_was_vowel and not is_vowel:
                draw_phoneme(to_draw, draw_pos, False, True)
            else:
                draw_phoneme(to_draw, draw_pos, False, False)

        phonemes_drawn += 1
        if phonemes_drawn > 1:
            phonemes_drawn = 0
            draw_pos.x += 4 * draw_scale

        if next_is_space:
            if phonemes_drawn == 1:
                draw_pos.x += 2 * space_dist
                phonemes_drawn = 0
                check_pos()

    elif not dont_check_space:
        draw_pos.x += space_dist
        phonemes_drawn = 0
        check_pos()

    dont_check_space = False
    previous_was_vowel = is_vowel

gl.update()

while True:
    pass