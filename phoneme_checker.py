database = ""
words = []
phonetics = []

# Copyright info (please don't sue me CMU)
print("Phonemes provided by Carnegie Mellon University (C) 2015, refer to database file for more information.")

def start_database():
    database = open("cmudict-0.7b", encoding = "ISO-8859-1")

    for line in database:
        if line[0] != ';':
            split_line = line.split("  ")
            words.append(split_line[0])
            phonetics.append(split_line[1].strip())
            
def query():
    query = input("Query: ").strip().upper()
    split_query = query.split(" ")
    phonetic_result = ""

    for word in split_query:
        phonemes_list = phonetics[words.index(word)].split()
        for phoneme in phonemes_list:
            if len(phoneme) > 2:
                phonetic_result += phoneme[0:2] + " "
            else:
                phonetic_result += phoneme + " "

        phonetic_result += "_ "

    return phonetic_result